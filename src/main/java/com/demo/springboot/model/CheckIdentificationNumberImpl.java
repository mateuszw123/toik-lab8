package com.demo.springboot.model;

public class CheckIdentificationNumberImpl  {
    public boolean checkIdentificationNumber(String id) {
        if(id.length()==11){
            int[] checksum_array = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};
            int sum;
            int tmpnumber;
            int control_number;
            int control_number_counted;
            sum=0;
            for (int i = 0; i < 10; i++){
                tmpnumber = Integer.parseInt(id.substring(i,i+1));
                sum = sum + tmpnumber * checksum_array[i];
            }
            control_number_counted=sum%10;
            control_number=Integer.parseInt(id.substring(10,11));
            if(control_number==control_number_counted){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}
